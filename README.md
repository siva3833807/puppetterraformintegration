# Terraform README

This README provides an overview and explanation of the Terraform code in this repository.

## Note

Puppet Master needs to manually created.

## Prerequisites

Before using this Terraform code, ensure that you have the following prerequisites:

- Terraform installed on your local machine
- Puppetmaster running either in VM or AWS EC2 instance
- Access to the cloud provider (e.g., AWS, Azure) where the infrastructure will be provisioned
- Proper authentication and access credentials for the cloud provider

# Puppet Master Setup on AWS EC2 instance

```
sudo nano /etc/hosts
```

Add the following line:

```
<Puppet_Master_Private_IP> puppet
```

### Download and install the Puppet Master packages

```
curl -O https://apt.puppetlabs.com/puppet6-release-bionic.deb
```
```
sudo dpkg -i puppet6-release-bionic.deb
```
```
sudo apt-get update
```
```
sudo apt-get install puppetserver -y
```
## Add changes
```
sudo nano /etc/default/puppetserver
```
```
JAVA_ARGS = "-Xms512m -Xms512m"
```
### Allow traffic on port 8140

```
sudo ufw allow 8140
```

### Configure and start Puppet Master

```
sudo systemctl enable puppetserver.service
```
```
sudo systemctl start puppetserver.service
```

### Verify the version of Puppet Master

```
sudo systemctl status puppetserver.service
```


## Getting Started

To get started with this Terraform code, follow these steps:

1. Clone this repository to your local machine.
2. Navigate to the appropriate directory containing the Terraform code.
3. Initialize the Terraform working directory by running `terraform init`.
4. Review the execution plan by running `terraform plan`.
5. Apply the changes by running `terraform apply`.
6. Meanwhile paste the init.pp file under the manifests directory of the puppet server.
7. After step 5 gets completed, sign the puppet agent certificate from the puppet master instance.

# Terraform

```
terraform init
```

```
terraform plan
```

```
terraform validate
```

```
terraform apply
```

## Usage

The terraform code will provision a new EC2 instance and automate the installation of Puppet Agent in it. After completing the installation, Puppet Agent service will be started and apply the catalog from Puppet-Server.



