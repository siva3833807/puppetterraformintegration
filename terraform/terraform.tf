# main.tf

provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "puppet-agent" {
  ami           = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.micro"
  key_name      = "my_user_keypair"
  vpc_security_group_ids = ["sg-00813b9bb098da7b9"]
  tags = {
    Name = "puppet-agent"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("my_user_keypair.pem")
    host        = self.public_ip
    timeout     = "5m" # Adjust timeout as per your requirement
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo wget https://apt.puppetlabs.com/puppet8-release-bionic.deb",
      "sudo dpkg -i puppet8-release-bionic.deb",
      "sudo apt update",
      "sudo apt install puppet-agent -y",
      "sudo sh -c 'echo \"172.31.16.152 puppet\" >> /etc/hosts'",
      "sudo systemctl enable puppet",
      "sudo systemctl start puppet",
      "sudo /opt/puppetlabs/bin/puppet agent --test"


    ]
  }
}

output "public_ip" {
  value = aws_instance.puppet-agent.public_ip
}
